import 'package:flutter/material.dart';
import 'page/IndexPage.dart';
void main () => runApp(MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
        child: MaterialApp(
          title: '百姓生活+',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primaryColor: Colors.lightBlue
          ),
          home: IndexPage(),
        ),
    );
  }
}