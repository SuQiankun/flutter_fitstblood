import 'package:flutter_first_blood/generated/json/base/json_convert_content.dart';

class TestEntity with JsonConvert<TestEntity> {
	int status;
	String message;
	TestData data;
}

class TestData with JsonConvert<TestData> {
	String token;
}
