class Autogenerated {
  int code;
  Data data;

  Autogenerated({this.code, this.data});

  Autogenerated.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String status;
  String userName;
  String userToken;
  String phoneNumber;
  String birthDay;
  List<String> habit;
  SportDataInfo sportDataInfo;

  Data(
      {this.status,
        this.userName,
        this.userToken,
        this.phoneNumber,
        this.birthDay,
        this.habit,
        this.sportDataInfo});

  Data.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    userName = json['userName'];
    userToken = json['userToken'];
    phoneNumber = json['phoneNumber'];
    birthDay = json['birthDay'];
    habit = json['habit'].cast<String>();
    sportDataInfo = json['sportDataInfo'] != null
        ? new SportDataInfo.fromJson(json['sportDataInfo'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['userName'] = this.userName;
    data['userToken'] = this.userToken;
    data['phoneNumber'] = this.phoneNumber;
    data['birthDay'] = this.birthDay;
    data['habit'] = this.habit;
    if (this.sportDataInfo != null) {
      data['sportDataInfo'] = this.sportDataInfo.toJson();
    }
    return data;
  }
}

class SportDataInfo {
  AbdominalWheel abdominalWheel;

  SportDataInfo({this.abdominalWheel});

  SportDataInfo.fromJson(Map<String, dynamic> json) {
    abdominalWheel = json['Abdominal wheel'] != null
        ? new AbdominalWheel.fromJson(json['Abdominal wheel'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.abdominalWheel != null) {
      data['Abdominal wheel'] = this.abdominalWheel.toJson();
    }
    return data;
  }
}

class AbdominalWheel {
  String avgCount;
  String lastTime;
  String lastCount;

  AbdominalWheel({this.avgCount, this.lastTime, this.lastCount});

  AbdominalWheel.fromJson(Map<String, dynamic> json) {
    avgCount = json['avgCount'];
    lastTime = json['lastTime'];
    lastCount = json['lastCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['avgCount'] = this.avgCount;
    data['lastTime'] = this.lastTime;
    data['lastCount'] = this.lastCount;
    return data;
  }
}
