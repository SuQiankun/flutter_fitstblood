import 'package:flutter_first_blood/generated/json/base/json_convert_content.dart';
import 'package:flutter_first_blood/generated/json/base/json_filed.dart';

class Resada with JsonConvert<Resada> {
	String accountId;
	String avatar;
	String nickname;
	String gender;
	String birthday;
	String weight;
	String bust;
	String waist;
	String hipline;
	String height;
	String bmi;
	String thighCircumference;
	@JSONField(name: "CalfCircumference")
	String calfCircumference;
	String upperArm;
	String fatr;
	String province;
	String city;
	String country;
	String ynionid;
	String privilege;
	String modifyTime;
}
