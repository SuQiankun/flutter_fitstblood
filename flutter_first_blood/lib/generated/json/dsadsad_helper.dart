import 'package:flutter_first_blood/generated/dsadsad.dart';

dsadsadFromJson(Dsadsad data, Map<String, dynamic> json) {
	if (json['name'] != null) {
		data.name = json['name']?.toString();
	}
	if (json['age'] != null) {
		data.age = json['age']?.toInt();
	}
	if (json['singer'] != null) {
		data.singer = json['singer']?.toString();
	}
	return data;
}

Map<String, dynamic> dsadsadToJson(Dsadsad entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['name'] = entity.name;
	data['age'] = entity.age;
	data['singer'] = entity.singer;
	return data;
}