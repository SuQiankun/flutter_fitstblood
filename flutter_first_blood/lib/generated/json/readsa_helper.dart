import 'package:flutter_first_blood/generated/readsa.dart';

readsaFromJson(Readsa data, Map<String, dynamic> json) {
	if (json['profile'] != null) {
		data.profile = new ReadsaProfile().fromJson(json['profile']);
	}
	if (json['token'] != null) {
		data.token = json['token']?.toString();
	}
	return data;
}

Map<String, dynamic> readsaToJson(Readsa entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	if (entity.profile != null) {
		data['profile'] = entity.profile.toJson();
	}
	data['token'] = entity.token;
	return data;
}

readsaProfileFromJson(ReadsaProfile data, Map<String, dynamic> json) {
	if (json['AccountId'] != null) {
		data.accountId = json['AccountId']?.toString();
	}
	if (json['Avatar'] != null) {
		data.avatar = json['Avatar']?.toString();
	}
	if (json['Nickname'] != null) {
		data.nickname = json['Nickname']?.toString();
	}
	if (json['Gender'] != null) {
		data.gender = json['Gender']?.toString();
	}
	if (json['Birthday'] != null) {
		data.birthday = json['Birthday']?.toString();
	}
	if (json['Weight'] != null) {
		data.weight = json['Weight']?.toString();
	}
	if (json['Bust'] != null) {
		data.bust = json['Bust']?.toString();
	}
	if (json['Waist'] != null) {
		data.waist = json['Waist']?.toString();
	}
	if (json['Hipline'] != null) {
		data.hipline = json['Hipline']?.toString();
	}
	if (json['Height'] != null) {
		data.height = json['Height']?.toString();
	}
	if (json['Bmi'] != null) {
		data.bmi = json['Bmi']?.toString();
	}
	if (json['Thighcircumference'] != null) {
		data.thighcircumference = json['Thighcircumference']?.toString();
	}
	if (json['Calfcircumference'] != null) {
		data.calfcircumference = json['Calfcircumference']?.toString();
	}
	if (json['Upper_arm'] != null) {
		data.upperArm = json['Upper_arm']?.toString();
	}
	if (json['Fatr'] != null) {
		data.fatr = json['Fatr']?.toString();
	}
	if (json['Province'] != null) {
		data.province = json['Province']?.toString();
	}
	if (json['City'] != null) {
		data.city = json['City']?.toString();
	}
	if (json['Country'] != null) {
		data.country = json['Country']?.toString();
	}
	if (json['Unionid'] != null) {
		data.unionid = json['Unionid']?.toString();
	}
	if (json['Privilege'] != null) {
		data.privilege = json['Privilege']?.toString();
	}
	if (json['ModifyTime'] != null) {
		data.modifyTime = json['ModifyTime']?.toString();
	}
	return data;
}

Map<String, dynamic> readsaProfileToJson(ReadsaProfile entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['AccountId'] = entity.accountId;
	data['Avatar'] = entity.avatar;
	data['Nickname'] = entity.nickname;
	data['Gender'] = entity.gender;
	data['Birthday'] = entity.birthday;
	data['Weight'] = entity.weight;
	data['Bust'] = entity.bust;
	data['Waist'] = entity.waist;
	data['Hipline'] = entity.hipline;
	data['Height'] = entity.height;
	data['Bmi'] = entity.bmi;
	data['Thighcircumference'] = entity.thighcircumference;
	data['Calfcircumference'] = entity.calfcircumference;
	data['Upper_arm'] = entity.upperArm;
	data['Fatr'] = entity.fatr;
	data['Province'] = entity.province;
	data['City'] = entity.city;
	data['Country'] = entity.country;
	data['Unionid'] = entity.unionid;
	data['Privilege'] = entity.privilege;
	data['ModifyTime'] = entity.modifyTime;
	return data;
}