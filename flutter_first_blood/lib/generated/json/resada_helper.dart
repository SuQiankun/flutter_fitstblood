import 'package:flutter_first_blood/generated/resada.dart';

resadaFromJson(Resada data, Map<String, dynamic> json) {
	if (json['accountId'] != null) {
		data.accountId = json['accountId']?.toString();
	}
	if (json['avatar'] != null) {
		data.avatar = json['avatar']?.toString();
	}
	if (json['nickname'] != null) {
		data.nickname = json['nickname']?.toString();
	}
	if (json['gender'] != null) {
		data.gender = json['gender']?.toString();
	}
	if (json['birthday'] != null) {
		data.birthday = json['birthday']?.toString();
	}
	if (json['weight'] != null) {
		data.weight = json['weight']?.toString();
	}
	if (json['bust'] != null) {
		data.bust = json['bust']?.toString();
	}
	if (json['waist'] != null) {
		data.waist = json['waist']?.toString();
	}
	if (json['hipline'] != null) {
		data.hipline = json['hipline']?.toString();
	}
	if (json['height'] != null) {
		data.height = json['height']?.toString();
	}
	if (json['bmi'] != null) {
		data.bmi = json['bmi']?.toString();
	}
	if (json['thighCircumference'] != null) {
		data.thighCircumference = json['thighCircumference']?.toString();
	}
	if (json['CalfCircumference'] != null) {
		data.calfCircumference = json['CalfCircumference']?.toString();
	}
	if (json['upperArm'] != null) {
		data.upperArm = json['upperArm']?.toString();
	}
	if (json['fatr'] != null) {
		data.fatr = json['fatr']?.toString();
	}
	if (json['province'] != null) {
		data.province = json['province']?.toString();
	}
	if (json['city'] != null) {
		data.city = json['city']?.toString();
	}
	if (json['country'] != null) {
		data.country = json['country']?.toString();
	}
	if (json['ynionid'] != null) {
		data.ynionid = json['ynionid']?.toString();
	}
	if (json['privilege'] != null) {
		data.privilege = json['privilege']?.toString();
	}
	if (json['modifyTime'] != null) {
		data.modifyTime = json['modifyTime']?.toString();
	}
	return data;
}

Map<String, dynamic> resadaToJson(Resada entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['accountId'] = entity.accountId;
	data['avatar'] = entity.avatar;
	data['nickname'] = entity.nickname;
	data['gender'] = entity.gender;
	data['birthday'] = entity.birthday;
	data['weight'] = entity.weight;
	data['bust'] = entity.bust;
	data['waist'] = entity.waist;
	data['hipline'] = entity.hipline;
	data['height'] = entity.height;
	data['bmi'] = entity.bmi;
	data['thighCircumference'] = entity.thighCircumference;
	data['CalfCircumference'] = entity.calfCircumference;
	data['upperArm'] = entity.upperArm;
	data['fatr'] = entity.fatr;
	data['province'] = entity.province;
	data['city'] = entity.city;
	data['country'] = entity.country;
	data['ynionid'] = entity.ynionid;
	data['privilege'] = entity.privilege;
	data['modifyTime'] = entity.modifyTime;
	return data;
}