import 'package:flutter_first_blood/generated/json/base/json_convert_content.dart';
import 'package:flutter_first_blood/generated/json/base/json_filed.dart';

class Readsa with JsonConvert<Readsa> {
	ReadsaProfile profile;
	String token;
}

class ReadsaProfile with JsonConvert<ReadsaProfile> {
	@JSONField(name: "AccountId")
	String accountId;
	@JSONField(name: "Avatar")
	String avatar;
	@JSONField(name: "Nickname")
	String nickname;
	@JSONField(name: "Gender")
	String gender;
	@JSONField(name: "Birthday")
	String birthday;
	@JSONField(name: "Weight")
	String weight;
	@JSONField(name: "Bust")
	String bust;
	@JSONField(name: "Waist")
	String waist;
	@JSONField(name: "Hipline")
	String hipline;
	@JSONField(name: "Height")
	String height;
	@JSONField(name: "Bmi")
	String bmi;
	@JSONField(name: "Thighcircumference")
	String thighcircumference;
	@JSONField(name: "Calfcircumference")
	String calfcircumference;
	@JSONField(name: "Upper_arm")
	String upperArm;
	@JSONField(name: "Fatr")
	String fatr;
	@JSONField(name: "Province")
	String province;
	@JSONField(name: "City")
	String city;
	@JSONField(name: "Country")
	String country;
	@JSONField(name: "Unionid")
	String unionid;
	@JSONField(name: "Privilege")
	String privilege;
	@JSONField(name: "ModifyTime")
	String modifyTime;
}
