import 'dart:math';
import 'package:dio/dio.dart';
import 'dart:io';
import 'dart:async';
import 'package:flutter/cupertino.dart';

/// 基础的请求url, 后面跟拼接的方法名; 可配置多个环境下的baseURL
const String netBaseURL =  'http://39.104.87.191/api/v1/';

/// 请求超时时间
const int netRequestTimeOut = 5000;

/// 接受后端返回数据的超时时间
const int netReceiveTimeOut = 5000;

/// 请求框架的配置项
BaseOptions opt = new BaseOptions(baseUrl: netBaseURL ,connectTimeout: netRequestTimeOut,receiveTimeout: netReceiveTimeOut,);

/// 初始化请求工具
Dio dioTool =  new Dio(opt);


/// *以下下是 [netBaseURL] 后拼接的请求方法

/// 登录路径
const String netLoginPath = 'userlogin';

/// 获取验证码路径
const String netGetAuthCodePath = 'mescode';

/// 第三方登录路径
const String netThirdServiceLoginPath = 'thirdlogin';

/// 获取用户信息路径
const String netGetUserInfoPath = 'getuserinfo';

/// 更新用户信息路径
const String netUpdateUserInfoPath = 'updateuserinfo';


/// * 以上是 [netBaseURL] 后拼接的请求方法


/// 获取验证码
/// * params 传递进来的参数
Future netGetAuthCode (var params) async {
  try{
    Response response = await dioTool.post(netBaseURL + netGetAuthCodePath,queryParameters: params);
    print('---------------${response.data}');
    return response.data;
  }catch(e){

  }
}

/// 登录请求
/// * params 传递进来的参数
Future netLoginAction (var params) async{

  try{
    Response response = await dioTool.post(netBaseURL + netLoginPath,queryParameters: params);
    print('---------------${response.data}');
    return response.data;

  }catch(e){

  }
}
/// 第三方登录行为
/// * params 后台需要获取的参数
Future netThirdServiceLoginAction (var params) async {
  try{
    Response response = await dioTool.post(netBaseURL + netThirdServiceLoginPath,queryParameters: params);
    print('---------------${response.data}');
    return response.data;

  }catch(e){

  }
}

/// 获取用户信息
/// * params 后端需要的参数
Future netGetUserInfoAction (var params) async {
  try{
    Response response = await dioTool.post(netBaseURL + netGetUserInfoPath,queryParameters: params);
    print('---------------${response.data}');
    return response.data;

  }catch(e){

  }
}

/// 更新用户信息
/// * params 后端需要的参数
Future netUpdateUserInfoAction (var params) async {
  try{
    Response response = await dioTool.post(netBaseURL + netUpdateUserInfoPath,queryParameters: params);
    print('---------------${response.data}');
    return response.data;
  }catch(e){

  }
}


