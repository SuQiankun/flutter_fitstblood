import 'dart:ffi';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:cipher2/cipher2.dart';


// login 

import '../Tools/netTool.dart';
// 输入密码的提示文字
String  pswPlaceHolderString = '请输入密码:';


class LoginClass extends StatefulWidget {
  LoginClass({Key key}) : super(key: key);

  @override
  LoginClassState createState() => LoginClassState();
}

class LoginClassState extends State<LoginClass> {
  
  var pressedValue;

  
  

  /// 手机号 
  TextEditingController _userNameController = TextEditingController();
  /// 密码输入框
  TextEditingController _pswNameController = TextEditingController();
  

  bool statusValue = false;

  
  
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       body:  returnWidgetTree()
       );
  }

Widget returnWidgetTree (){
 List <Widget> widgetTree ;
if (statusValue) {
  widgetTree = [UnconstrainedBox(
                      child:Container(
                        padding: const EdgeInsets.fromLTRB(10, 0, 0, 10),
                        color: Colors.red,
                        child:  CupertinoButton(
                          disabledColor: Colors.black,
                          child: Text('切换密码登录',textAlign: TextAlign.right,),
                          onPressed: (){},
                        ),
                      ),
                    ),
                    Text(
                      '登录',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 30.0),
                    ),
                    Container(
                      child: TextField(
                        decoration: const InputDecoration(hintText: '请输入手机号:'),
                        controller: _userNameController,
                        onChanged: (value){changeValue(value);},
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                    
                      child: TextField(
                        decoration:  InputDecoration(hintText: pswPlaceHolderString),
                        controller: _pswNameController,
                        obscureText: true,
                        onChanged: (value){changeValue(value);},
                      ),
                    ),];
} else {
   widgetTree =  [
                    UnconstrainedBox(
                      child:Container(
                        padding: const EdgeInsets.fromLTRB(10, 0, 0, 10),
                        color: Colors.red,
                        child:  CupertinoButton(
                          disabledColor: Colors.black,
                          child: Text('切换密码登录',textAlign: TextAlign.right,),
                          onPressed: (){},
                        ),
                      ),
                    ),
                    Text(
                      '登录',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 30.0),
                    ),
                    Container(
                      child: TextField(
                        decoration: const InputDecoration(hintText: '请输入手机号:'),
                        controller: _userNameController,
                        onChanged: (value){changeValue(value);},
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                    
                      child: TextField(
                        decoration:  InputDecoration(hintText: pswPlaceHolderString),
                        controller: _pswNameController,
                        obscureText: true,
                        onChanged: (value){changeValue(value);},
                      ),
                    ),
                  Padding(
                     padding: EdgeInsets.symmetric(horizontal:90.0),
                    child:CupertinoButton(
                      disabledColor: Colors.black,
                      child: Text('登录'),
                      onPressed: (){
                        testFunction();
                      }),
                    ),
              ];
  
}
 
  return Padding(
    padding: EdgeInsets.symmetric(horizontal:34.0),
    child: Column(
        children: widgetTree,
    ),
  );

  // return widgetTree;
}

void testFunction(){
setState(() {
  statusValue = !statusValue;
  returnWidgetTree();
});

}

void changeValue(value){
    String phoneNum = _userNameController.text;
    String psw = _pswNameController.text;

    if(phoneNum.length != 0 && psw.length != 0){
     setState(() {
         pressedValue = (){

           loginAction().then((value){
             print('$value');
           });
//           String tipString ;
//           netGetAuthCode({'phone':'15509001276','code':'762093','password':''}).then((value){
//             print('请求结果: $value');
//             tipString = value['message'];
//             showDialog(
//                 context: context,
//                 builder: (context) => AlertDialog(title: Text('$tipString'),)
//             );
//           });
         };
       });
    }else{
      setState(() {
        pressedValue = null;
      });
    }
 }

 Future loginAction() async {

   String encryptedString = await Cipher2.encryptAesCbc128Padding7('admin', 'johnson_shanghai', 'johnson_shanghai');
   print('加密后:{$encryptedString}');
   return encryptedString;
 }

 Future getAuthCode() async {
   Dio dioRequest =  new Dio();
   dioRequest.options
     ..baseUrl =  "http://39.104.87.191/api/v1"
     ..connectTimeout = 5000 //5s
     ..receiveTimeout = 5000;

   Response res = await dioTool.post('/mescode', data: {'phone':_userNameController.text,'mode':'register_mode'});
   return res.data;
 }
}
