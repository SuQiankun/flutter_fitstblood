import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'classes_page.dart';
import 'profile_page.dart';
import 'plan_page.dart';
import 'sport_page.dart';
import 'login_page.dart';

class IndexPage extends StatefulWidget {
  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {

  final List tabbarClass = [
    ClassPage(),
    SportPage(),
    PlanPage(),
    LoginClass(),
  ];


  int currentIndexValue = 0;
  var currentPage;


  final List<BottomNavigationBarItem> bottomList = [
     BottomNavigationBarItem(icon: Icon(CupertinoIcons.book_solid),title:Text('运动')),
     BottomNavigationBarItem(icon: Icon(CupertinoIcons.time),title:Text('课程')),
     BottomNavigationBarItem(icon: Icon(CupertinoIcons.person),title:Text('计划')),
     BottomNavigationBarItem(icon: Icon(CupertinoIcons.profile_circled),title:Text('我')),
  ];


  @override
  void initState() {

    currentPage = tabbarClass[currentIndexValue];

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: currentIndexValue,
        items: bottomList,
        onTap: (index){
            setState(() {
              currentIndexValue = index;
              currentPage = tabbarClass[currentIndexValue];
            });
        },
      ),
      body: currentPage,
    );
  }
  
  

}
